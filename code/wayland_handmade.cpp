#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <wayland-client.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <errno.h>
#include <unistd.h>

#define internal static
#define local_persist static
#define global_variable static

typedef int8_t int8;
typedef int16_t int16;
typedef int32_t int32;
typedef int64_t int64;

typedef uint8_t uint8;
typedef uint16_t uint16;
typedef uint32_t uint32;
typedef uint64_t uint64;

struct wl_display *display = NULL;
struct wl_compositor *compositor = NULL;
struct wl_surface *surface;
struct wl_shell *shell;
struct wl_shell_surface *shell_surface;
struct wl_shm *shm;
struct wl_buffer *buffer;
struct wl_callback *frame_callback;

void *shm_data;

int WIDTH = 480;
int HEIGHT = 360;
bool Running = true;

global_variable int BytesPerPixel = 4;

static void
handle_ping(void *data, struct wl_shell_surface *shell_surface,
        uint32 serial)
{
    wl_shell_surface_pong(shell_surface, serial);
    fprintf(stderr, "Pinged and ponged\n");
}

static void
handle_configure(void *data, struct wl_shell_surface *shell_surface,
                 uint32 edges, int32 width, int32 height)
{
}

static void
handle_popup_done(void *data, struct wl_shell_surface *shell_surface)
{
}

static const struct wl_shell_surface_listener shell_surface_listener = {
    handle_ping,
    handle_configure,
    handle_popup_done
};

static int
set_cloexec_or_close(int fd)
{
    long flags;

    if (fd == -1) {
        return -1;
    }
    flags = fcntl(fd, F_GETFD);
    if (flags == -1) {
        goto err;
    }

    if (fcntl(fd, F_SETFD, flags | FD_CLOEXEC) == -1) {
        goto err;
    }

    return fd;

err:
    close(fd);
    return -1;
}

static int
create_tmpfile_cloexec(char *tmpname)
{
    int fd;
#ifdef HAVE_MKOSTEMP
    fd = mkostemp(tmpname, O_CLOEXEC);
    if (fd >= 0) {
        unlink(tmpname);
    }
#else
    fd = mkstemp(tmpname);
    if (fd >= 0) {
        fd = set_cloexec_or_close(fd);
        unlink(tmpname);
    }
#endif
    return fd;
}

int
os_create_anonymous_file(off_t size) 
{
    static const char template_wl[] = "/weston-shared-XXXXXX";
    const char *path;
    char *name;
    int fd;

    path = getenv("XDG_RUNTIME_DIR");
    if(!path) {
        errno = ENOENT;
        return -1;
    }

    name = (char*) malloc(strlen(path) + sizeof(template_wl));
    if (!name) {
        return -1;
    }

    strcpy(name, path);
    strcat(name, template_wl);

    fd = create_tmpfile_cloexec(name);
    free(name);
    if(fd < 0) {
        return -1;
    }
    if (ftruncate(fd, size) < 0) {
        close(fd);
        return -1;
    }
    return fd;
}

static void
paint_pixels(int BlueOffset, int GreenOffset) {
    int Pitch = WIDTH * BytesPerPixel;
    uint8 *Row = (uint8*) shm_data;
    for(int Y = 0; Y < HEIGHT; ++Y) {
        uint32 *Pixel = (uint32*) Row;

        for(int X = 0; X < WIDTH; ++X) {
            uint8 Blue = X + BlueOffset;
            uint8 Green = Y + GreenOffset;
            *Pixel++ = ((Green << 8) | Blue);
        }
        Row += Pitch;
    }

}

static void redraw(void *data, struct wl_callback *callback, uint32 time);

static const struct wl_callback_listener frame_listener = {
    redraw
};

static void
redraw(void *data, struct wl_callback *callback, uint32 time) {
    wl_callback_destroy(frame_callback);
    wl_surface_damage(surface, 0, 0, WIDTH, HEIGHT);
    frame_callback = wl_surface_frame(surface);
    wl_surface_attach(surface, buffer, 0, 0);
    wl_callback_add_listener(frame_callback, &frame_listener, NULL);
    wl_surface_commit(surface);
}


static struct wl_buffer *
create_buffer() {
    struct wl_shm_pool *pool;
    int stride = WIDTH * BytesPerPixel;
    int size = stride * HEIGHT;
    int fd;
    struct wl_buffer *buff;

    fd = os_create_anonymous_file(size);
    if (fd < 0) {
        fprintf(stderr, "createing a buffer file for %d B failed: %m\n", size);
        exit(1);
    }

    shm_data = mmap(NULL, size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    if(shm_data == MAP_FAILED) {
        fprintf(stderr, "mmap failed: %m\n");
        close(fd);
        exit(1);
    }

    pool = wl_shm_create_pool(shm, fd, size);
    buff = wl_shm_pool_create_buffer(pool, 0,
                                     WIDTH, HEIGHT,
                                     stride,
                                     WL_SHM_FORMAT_XRGB8888);
    wl_shm_pool_destroy(pool);
    return buff;
}

static void
create_window() {
    buffer = create_buffer();
    wl_surface_attach(surface, buffer, 0, 0);
    wl_surface_commit(surface);
}

static void
shm_format(void *data, struct wl_shm *wl_shm, uint32 format) {
    fprintf(stderr, "Format %d\n", format);
}

struct wl_shm_listener shm_listener = {
    shm_format
};

static void
global_registry_handler(void *data, struct wl_registry *registry, 
                        uint32_t id, const char *interface, 
                        uint32_t version)
{
    printf("Got a registry event for %s id %d\n", interface, id);
    if (strcmp(interface, "wl_compositor") == 0) {
        compositor = (wl_compositor*) wl_registry_bind(registry,
                                      id,
                                      &wl_compositor_interface,
                                      1);
    } else if (strcmp(interface, "wl_shell") == 0) {
        shell = (wl_shell*) wl_registry_bind(registry, id,
                                 &wl_shell_interface, 1);
    } else if (strcmp(interface, "wl_shm") == 0) {
        shm = (wl_shm*) wl_registry_bind(registry,
                                         id,
                                         &wl_shm_interface,
                                         1);
        wl_shm_add_listener(shm, &shm_listener, NULL);
    }
}

static void
global_registry_remover(void *data, struct wl_registry *registry, uint32_t id)
{
    printf("Got a registry losing event for %d\n", id);
}

static const struct wl_registry_listener registry_listener = {
    global_registry_handler,
    global_registry_remover
};

internal void
wl_update_window()
{
    wl_surface_commit(surface);
    wl_surface_damage(surface, 0, 0, WIDTH, HEIGHT);
}

int main(int argc, char **argv) {
    display = wl_display_connect(NULL);
    if (display == NULL) {
        fprintf(stderr, "Can't connect to display\n");
        exit(1);
    }
    printf("connected to display\n");

    struct wl_registry *registry = wl_display_get_registry(display);
    wl_registry_add_listener(registry, &registry_listener, NULL);

    wl_display_dispatch(display);
    wl_display_roundtrip(display);

    if (compositor == NULL) {
        fprintf(stderr, "Can't find compositor\n");
        exit(1);
    } else {
        fprintf(stderr, "Found compositor\n");
    }

    surface = wl_compositor_create_surface(compositor);
    if (surface == NULL) {
        fprintf(stderr, "Can't create surface\n");
        exit(1);
    } else {
        fprintf(stderr, "Created surface\n");
    }

    shell_surface = wl_shell_get_shell_surface(shell, surface);
    if (shell_surface == NULL) {
        fprintf(stderr, "Can't create shell surface\n");
        exit(1);
    } else {
        fprintf(stderr, "Created shell surface\n");
    }

    wl_shell_surface_set_toplevel(shell_surface);
    wl_shell_surface_add_listener(shell_surface,
                                  &shell_surface_listener,
                                  NULL);

    frame_callback = wl_surface_frame(surface);
    wl_callback_add_listener(frame_callback, &frame_listener, NULL);

    int BlueOffset = 0;
    int GreenOffset = 0;
    create_window();
    paint_pixels(BlueOffset, GreenOffset);

    while (Running) {
        paint_pixels(BlueOffset, GreenOffset);
        BlueOffset++;
        wl_display_dispatch(display);
    }

    wl_display_disconnect(display);
    printf("disconnected from display\n");
    exit(0);
}
