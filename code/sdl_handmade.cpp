#include <SDL.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <stdint.h>

#define internal static
#define local_persist static
#define global_variable static

typedef int8_t int8;
typedef int16_t int16;
typedef int32_t int32;
typedef int64_t int64;

typedef uint8_t uint8;
typedef uint16_t uint16;
typedef uint32_t uint32;
typedef uint64_t uint64;

global_variable bool Running;
global_variable SDL_Texture *Texture;
global_variable void *BitmapMemory;
global_variable int BitmapWidth;
global_variable int BitmapHeight;
global_variable int BytesPerPixel = 4;

internal void RenderWeirdGradient(int BlueOffset, int GreenOffset) {
    int Width = BitmapWidth;
    int Height = BitmapHeight;

    int Pitch = Width * BytesPerPixel;
    uint8 *Row = (uint8 *)BitmapMemory;
    for(int Y = 0; Y < BitmapHeight; ++Y) {
        uint32 *Pixel = (uint32 *)Row;

        for(int X = 0; X < BitmapWidth; ++X) {
            uint8 Blue = (X + BlueOffset);
            uint8 Green = (Y + GreenOffset);
            *Pixel++ = ((Green << 8) | Blue);
        }
        Row += Pitch;
    }
}

internal void SDLResizeTexture(SDL_Renderer *Renderer, int Width, int Height) {
    if (BitmapMemory) {
        munmap(BitmapMemory, BitmapWidth * BitmapHeight * BytesPerPixel);
    }

    if (Texture) {
        SDL_DestroyTexture(Texture);
    }

    Texture = SDL_CreateTexture(Renderer,
                 SDL_PIXELFORMAT_ARGB8888,
                 SDL_TEXTUREACCESS_STREAMING,
                 Width,
                 Height);

    BitmapWidth = Width;
    BitmapHeight = Height;
    BitmapMemory = mmap(0, 
                        Width * Height * BytesPerPixel,
                        PROT_READ|PROT_WRITE,
                        MAP_ANONYMOUS|MAP_PRIVATE,
                        -1,
                        0);
}

internal void
SDLUpdateWindow(SDL_Window *Window, SDL_Renderer *Renderer) {
    SDL_UpdateTexture(Texture,
                      0,
                      BitmapMemory,
                      BitmapWidth * BytesPerPixel);

    SDL_RenderCopy(Renderer,
                   Texture,
                   0,
                   0);

    SDL_RenderPresent(Renderer);
}

void HandleEvent(SDL_Event *Event) {
    switch(Event->type){
        case SDL_QUIT:
        {
            Running = false;
        }

        case SDL_WINDOWEVENT:
        {
            switch(Event->window.event) {
                case SDL_WINDOWEVENT_SIZE_CHANGED:
                {
                    SDL_Window *Window = SDL_GetWindowFromID(Event->window.windowID);
                    SDL_Renderer *Renderer = SDL_GetRenderer(Window);
                    SDLResizeTexture(Renderer, Event->window.data1, Event->window.data2);
                    break;
                }

                case SDL_WINDOWEVENT_EXPOSED:
                {
                    SDL_Window *Window = SDL_GetWindowFromID(Event->window.windowID);
                    SDL_Renderer *Renderer = SDL_GetRenderer(Window);
                    SDLUpdateWindow(Window, Renderer);
                    break;
                }
            }
            break;
        }
    }
}

int main(int argc, char *argv[]) {
    //SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_INFORMATION, 
            //"Handmade Heror", "This is Handmade Hero", 0);
            //
    if (SDL_Init(SDL_INIT_VIDEO) != 0) {
        //TODO: SDL_Init didn't work
    }

    SDL_Window *Window = SDL_CreateWindow(
                            "Handmade Hero", 
                            SDL_WINDOWPOS_UNDEFINED,
                            SDL_WINDOWPOS_UNDEFINED,
                            640,
                            480,
                            SDL_WINDOW_RESIZABLE);
    if (Window) {
        Running = true;
        int XOffset = 0;
        int YOffset = 0;
        SDL_Renderer *Renderer = SDL_CreateRenderer(Window, -1, 0);
        if (Renderer) {
            int Width, Height;
            SDL_GetWindowSize(Window, &Width, &Height);
            SDLResizeTexture(Renderer, Width, Height);

            while(Running) {
                SDL_Event Event;
                while(SDL_PollEvent(&Event)) {
                    HandleEvent(&Event);
                }
                RenderWeirdGradient(XOffset, YOffset);
                SDLUpdateWindow(Window, Renderer);

                XOffset++;
            }
            atexit(SDL_Quit);
        }
    }
    
    SDL_Quit();
    return(0);
}
